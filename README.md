# histo-proteo

Core code for histology-based deep learning model reported in:

**Connecting Histopathology Imaging and Proteomics in Kidney Cancer through Machine Learning **

Francisco Azuaje, Sang-Yoon Kim, Daniel Perez Hernandez and Gunnar Dittmar
Quantitative Biology Unit, Luxembourg Institute of Health (LIH), Strassen, Luxembourg.

Code developed by Francisco Azuaje.
